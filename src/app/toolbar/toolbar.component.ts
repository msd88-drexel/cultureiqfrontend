import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements AfterViewInit {
  public activeModule = 'Translator';
  public activeModuleEmitter: EventEmitter<string> = new EventEmitter();

  constructor() {
  }

  changeModule(moduleName: string) {
    this.activeModule = moduleName;
  }

  ngAfterViewInit() {
    this.activeModuleEmitter.emit(this.activeModule);
    this.activeModuleEmitter.subscribe( activeModuleString => this.activeModule = activeModuleString);
  }

}
