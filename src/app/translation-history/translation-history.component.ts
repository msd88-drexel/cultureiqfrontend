import { Component, OnInit } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import {TranslateService} from "../translate.service";

@Component({
  selector: 'app-translation-history',
  templateUrl: './translation-history.component.html',
  styleUrls: ['./translation-history.component.css']
})
export class TranslationHistoryComponent implements OnInit {
  displayedColumns = ['request_date', 'source_tx', 'src_lang_tx', 'trans_tx', 'dst_lang_tx'];
  exampleDatabase;
  dataSource: ExampleDataSource | null;

  constructor(private translationService: TranslateService) {}

  ngOnInit() {
    this.exampleDatabase = new Database(this.translationService);
    this.dataSource = new ExampleDataSource(this.exampleDatabase);
  }
}

export interface TranslationData {
  request_date: Date;
  source_tx: string;
  trans_tx: string;
  src_lang_tx: string;
  dst_lang_tx: string;
}

export class Database {
  dataChange: BehaviorSubject<TranslationData[]> = new BehaviorSubject<TranslationData[]>([]);
  get data(): TranslationData[] { return this.dataChange.value; }

  constructor(private translationService: TranslateService ) {
    this.translationService.translationHistory().subscribe(response => {
      (response as Array<object>).map(x => this.addData(x as TranslationData));
    });
  }

  addData(dataRow: TranslationData) {
    const copiedData = this.data.slice();
    copiedData.push(dataRow);
    this.dataChange.next(copiedData);
  }

}

export class ExampleDataSource extends DataSource<any> {
  constructor(private _exampleDatabase: Database) {
    super();
  }

  connect(): Observable<TranslationData[]> {
    return this._exampleDatabase.dataChange;
  }

  disconnect() {}
}
