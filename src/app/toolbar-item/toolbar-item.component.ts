import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-toolbar-item',
  templateUrl: './toolbar-item.component.html',
  styleUrls: ['./toolbar-item.component.css']
})
export class ToolbarItemComponent implements AfterViewInit {
  @Input() public componentLinkName: string;
  @Input() public changeModuleEmitter: EventEmitter<string>;
  public activeComponent: boolean;

  constructor() { }

  ngAfterViewInit() {
    this.changeModuleEmitter.subscribe(activeModuleName => {
      this.activeComponent = this.componentLinkName === activeModuleName;
    });
  }

  handleClick(event: MouseEvent) {
    this.changeModuleEmitter.emit(this.componentLinkName);
  }
}
