/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

interface LanguageList {
  languages: Language[];
}

interface Language {
  language: string;
  name: string;
}

interface TranslateRequest {
  src_tx: string;
  src_lang: string;
  dst_lang: string;
}
