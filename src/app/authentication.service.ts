import { Injectable } from '@angular/core';
import { JwtHelper } from 'angular2-jwt';
import { HttpClient } from '@angular/common/http';
import {environment} from '../environments/environment';
import {Router} from '@angular/router';


@Injectable()
export class AuthenticationService {

  constructor(
    private jwtHelper: JwtHelper,
    private httpCleint: HttpClient,
    private router: Router) { }

  public isAuthenticated(): boolean {
    const token = localStorage.getItem('token');

    if (token == null) {
      return false;
    }
    // Check whether the token is expired and return
    // true or false
    return !this.jwtHelper.isTokenExpired(token);
  }

  public getJwtToken(username: string, password: string) {
    return this.httpCleint.post(
      environment.backend_uri + '/api-token-auth',
      {'username': username, 'password': password}
      );
  }

}
