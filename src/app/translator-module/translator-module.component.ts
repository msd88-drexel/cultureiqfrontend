import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, FormsModule} from '@angular/forms';
import {TranslateService} from '../translate.service';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-translator-module',
  templateUrl: './translator-module.component.html',
  styleUrls: ['./translator-module.component.css'],
})
export class TranslatorModuleComponent implements OnInit {

  public source_lang = {
    language: '',
    name: 'Enter Text!'
  };
  public source_tx: string;
  public translated_tx: string;
  public destLanguage: string;
  public validLanguages: Language[];

  private languages: Language[];
  private timeout: any;

  constructor(private translateService: TranslateService) {

  }

  ngOnInit() {
    this.translateService.getLanguages().subscribe(result => {
      this.languages = result['languages'];
    });
  }

  translate() {
    console.log('Translation input: ' + this.source_tx);
    console.log('Translation src language: ' + this.source_lang.name);
    console.log('Destination language: ' + this.destLanguage);

    const request: TranslateRequest = {
      src_tx: this.source_tx,
      src_lang: this.source_lang.name,
      dst_lang: this.destLanguage
    };

    this.translateService.translate(request).subscribe(response => {
      this.translated_tx = response as string;
      console.log(response);
    });

  }

  languageSearch(event: KeyboardEvent) {
    clearTimeout(this.timeout);
    const func = () => {
      this.translateService.detect_language(this.source_tx)
        .map(res => {
          this.source_lang = this.languages.find(lang => lang.language === res);
          return this.source_lang.language;
        })
        .mergeMap(language => this.translateService.getTranslatableLangs(language))
        .map(res => res as Array<string>)
        .subscribe( validLangs =>
          this.validLanguages = this.languages.filter(lang => validLangs.includes(lang.language)));
    };
    func.bind(this);
    this.timeout = setTimeout(func, 1000);
  }
}
