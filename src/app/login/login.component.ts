import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthenticationService} from "../authentication.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: any = {};
  showError = false;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {
  }

  login() {
    this.authenticationService.getJwtToken(this.model.username, this.model.password)
      .subscribe( result => {
        localStorage.setItem('token', result['token']);
        this.router.navigate(['translation']);
      },
      error => {
        this.showError =  true;
      });
  }
}
