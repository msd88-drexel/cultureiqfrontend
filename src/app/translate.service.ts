import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {RequestOptions} from '@angular/http';
import {Observable} from "rxjs/Observable";

@Injectable()
export class TranslateService {

  constructor(
    private httpService: HttpClient
  ) { }

  public getLanguages() {
    const languagePath = '/list_languages';
    const headers = new HttpHeaders({'Authorization': 'JWT ' + localStorage.getItem('token')});

    return this.httpService.get(environment.backend_uri + languagePath, {'headers': headers});
  }

  public translate(translateRequest: TranslateRequest) {
    const translatePath = '/translate';
    const headers = new HttpHeaders({'Authorization': 'JWT ' + localStorage.getItem('token')});

    return this.httpService.post(environment.backend_uri + translatePath, translateRequest, {'headers': headers});
  }

  public detect_language(translateRequest): Observable<Object> {
    const translatePath = '/detect_language';
    const headers = new HttpHeaders({'Authorization': 'JWT ' + localStorage.getItem('token')});

    return this.httpService.post(environment.backend_uri + translatePath, translateRequest, {'headers': headers});
  }

  public translationHistory() {
    const historyPath = '/translation_history';
    const headers = new HttpHeaders({'Authorization': 'JWT ' + localStorage.getItem('token')});

    return this.httpService.get(environment.backend_uri + historyPath, {'headers': headers});
  }

  public getTranslatableLangs(translateRequest) {
    const historyPath = '/translatable_langs';
    const headers = new HttpHeaders({'Authorization': 'JWT ' + localStorage.getItem('token')});

    return this.httpService.post(environment.backend_uri + historyPath, translateRequest, {'headers': headers});
  }

}
