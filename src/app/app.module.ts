import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ToolbarItemComponent } from './toolbar-item/toolbar-item.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { TranslatorModuleComponent } from './translator-module/translator-module.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateService} from './translate.service';
import { TranslationHistoryComponent } from './translation-history/translation-history.component';
import { MatTableModule } from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { AuthHttp, JwtHelper } from 'angular2-jwt';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './auth-guard.service';
import { AuthenticationService } from './authentication.service';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'translation',
    component: ToolbarComponent,
    canActivate: [AuthGuardService]
  },
  { path: '',
    redirectTo: '/translation',
    pathMatch: 'full'
  },
];


@NgModule({
  declarations: [
    AppComponent,
    ToolbarItemComponent,
    ToolbarComponent,
    TranslatorModuleComponent,
    TranslationHistoryComponent,
    LoginComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
    ),
    MatTableModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CdkTableModule,
  ],
  providers: [
    HttpClient,
    TranslateService,
    AuthenticationService,
    AuthHttp,
    AuthGuardService,
    JwtHelper,
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
